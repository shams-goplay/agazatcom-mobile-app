package com.shamsit.agazatcom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.pnikosis.materialishprogress.ProgressWheel;


public class MainActivity extends AppCompatActivity {

    WebView webView;
    LinearLayout no_connection;
    ProgressWheel progress_wheel;
    LinearLayout loading_layout;
    SwipeRefreshLayout swipe_refresh_layout;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        no_connection = (LinearLayout) findViewById(R.id.no_connection);
        loading_layout = (LinearLayout) findViewById(R.id.loading_layout);
        progress_wheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);


        webView = (WebView) findViewById(R.id.wb_view);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        if (isNetworkAvailable())
            startWebView("http://agazatcom.com/mobile-home-page/");
        else
            no_connection.setVisibility(View.VISIBLE);


        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshContent();
                progress_wheel.setVisibility(View.GONE);
            }
        });


    }

    private void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.reload();
                swipe_refresh_layout.setRefreshing(false);
            }
        }, 5000);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void startWebView(String url) {
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                no_connection.setVisibility(View.VISIBLE);
                loading_layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loading_layout.setVisibility(View.VISIBLE);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                loading_layout.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mms:") || url.startsWith("mmsto:")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else if (url.startsWith("mailto:")) {
                    Intent i = newEmailIntent(/*mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc()*/);
                    startActivity(i);
                    return true;

                } else if (url.contains("agazatcom.com")) {
                    view.loadUrl(url);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(i);
                }


                return false;
            }
        });
    }

    private Intent newEmailIntent(/*String address, String subject, String body, String cc*/) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        return intent;
    }

    @Override
    public void onStart() {
        super.onStart();

        swipe_refresh_layout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (webView.getScrollY() == 0)
                            swipe_refresh_layout.setEnabled(true);
                        else
                            swipe_refresh_layout.setEnabled(false);

                    }
                });
    }

    @Override
    public void onStop() {
        swipe_refresh_layout.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            //if Back key pressed and webview can navigate to previous page
            webView.goBack();
            // go back to previous page
            return true;
        } else {
            finish();
            // finish the activity
        }
        return super.onKeyDown(keyCode, event);
    }

}
